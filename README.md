- 基于`fastroute`做路由分发
- 使用轻量级ORM`illuminate/database`处理数据
- 通过composer集成各种必备组件
- 应用层做好分层处理，可扩展，代码复用



```
├── README.md
├── app
│   ├── Controllers
│   │   ├── BaseController.php
│   │   ├── Order
│   │   │   └── GoodsController.php
│   │   └── User
│   │       └── InfoController.php
│   └── Models
│       ├── Base
│       │   └── Dao.php
│       ├── Order
│       │   ├── Db
│       │   │   └── Goods.php
│       │   └── Services
│       │       └── Data.php
│       └── User
│           ├── Db
│           │   └── User.php
│           └── Services
│               └── Data.php
├── composer.json
├── conf
│   └── mysql.ini
└── public
    └── index.php

```