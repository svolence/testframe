<?php

namespace App\Models\Base;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

class Base_Dao extends Model
{
    public static function InitDB($dbName)
    {
        $config  = parse_ini_file(APP_PATH.'conf/mysql.ini');;
        if (empty($config)) {
            throw new Exception("Must configure database in .ini first");
        }

        $capsule = new Manager;
        $capsule->addConnection($config);
        $capsule->bootEloquent();

        if (!empty($config['env']) && $config['env'] != 'product') {
            //$capsule->getConnection()->enableQueryLog();

            $dispatcher = new Dispatcher(new Container);
            $capsule->setEventDispatcher($dispatcher);

            //Listening to all queries
            $capsule->getConnection()->listen(
                function ($query) {
                    foreach ($query->bindings as $i => $binding) {
                        if ($binding instanceof \DateTime) {
                            $query->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                        } else {
                            if (is_string($binding)) {
                                $query->bindings[$i] = "'$binding'";
                            }
                        }
                    }

                    $sql = str_replace(array('%', '?'), array('%%', '%s'), $query->sql);

                    \Log::debug(vsprintf($sql, $query->bindings));
                }
            );
        }
    }
}
