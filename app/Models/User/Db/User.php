<?php

namespace App\Models\User\Db;

class User extends Base_Dao
{
    const dbName = 'user';

    public $timestamps = false;

    protected $table = 't_user';

    protected $primaryKey = 'uid';



    public function __construct()
    {
        parent::__construct();
        Base_Dao::InitDB(self::dbName);
    }

    public static function getUserById($uid, $column = '*')
    {
        return self::where('uid', '=', $uid)->select($column)->get()->toArray();
    }
}
