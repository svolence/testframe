<?php

namespace App\Models\User\Services;

use App\Models\User\Db\User;

class Data
{
    public static function getUserInfoById($uid)
    {
        return User::find($uid);
    }
}