<?php

namespace App\Models\Order\Db;

class Goods extends Base_Dao
{
    const dbName = 'order';

    protected $table = 't_order_goods';

    public function __construct()
    {
        parent::__construct();
        Base_Dao::InitDB(self::dbName);
    }
}