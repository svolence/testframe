<?php

namespace App\Models\Order\Services;

use App\Models\Order\Db\Goods;


class Data
{
    public static function getUserOrderList($where, $page = 1, $length = 1000)
    {
        $obj = Goods::where($where);

        return [
            'data'  => $obj->offset($page)->limit($length)->get()->toArray(),
            'total' => $obj->count()
        ];
    }
}